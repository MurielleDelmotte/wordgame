# wordgame 0.0.1

- Add `complete_all_grid()`
- Add `complete_grid()`
- Add `init_grid()`
- Add `random_position()`
- Add `check_in_grid()`
- Add `check_available()`

