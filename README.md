
<!-- README.md is generated from README.Rmd. Please edit that file -->

# wordgame <img src="man/figures/hex-wordgame.png" align="right" alt="" width="120" />

<!-- badges: start -->

[![R build
status](https://gitlab.com/MurielleDelmotte/wordgame/badges/master/pipeline.svg)](https://gitlab.com/MurielleDelmotte/wordgame/-/pipelines)
[![coverage
report](https://gitlab.com/MurielleDelmotte/wordgame/badges/master/coverage.svg)](https://gitlab.com/MurielleDelmotte/wordgame/-/artifacts)
<!-- badges: end -->

The goal of wordgame is to create a grid of wordsearch.

## Installation

You can install the development version of wordgame like so:

``` r
remostes::install_gitlab("MurielleDelmotte/wordgame")
```

> *Full documentation is here:
> <https://murielledelmotte.gitlab.io/wordgame>*

## Example

To create a new grid with a list of words:

``` r
library(wordgame)
```

### Easy level

``` r
complete_all_grid(
  words = c("CHIEN", "RAT", "SOURIS", "CHAT", "POULE"),
  size_grid = 10, difficult_level = 1
)
```

|     |     |     |     |     |     |     |     |     |     |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|  F  |  T  |  N  |  Y  |  X  |  D  |  V  |  F  |  P  |  I  |
|  K  |  G  |  Z  |  P  |  R  |  S  |  E  |  S  |  L  |  M  |
|  C  |  O  |  S  |  S  |  T  |  O  |  W  |  C  |  L  |  U  |
|  K  |  L  |  N  |  U  |  U  |  U  |  M  |  H  |  C  |  H  |
|  Y  |  R  |  D  |  O  |  M  |  R  |  P  |  A  |  T  |  X  |
|  A  |  Q  |  Y  |  C  |  G  |  I  |  O  |  T  |  D  |  K  |
|  M  |  P  |  V  |  B  |  Z  |  S  |  U  |  R  |  A  |  T  |
|  J  |  L  |  F  |  U  |  W  |  Z  |  L  |  B  |  U  |  B  |
|  S  |  E  |  E  |  G  |  Z  |  U  |  E  |  A  |  W  |  O  |
|  B  |  A  |  R  |  C  |  H  |  I  |  E  |  N  |  V  |  G  |

The soluce:

|     |     |     |     |     |     |     |     |     |     |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|     |     |     |     |     |     |     |     |     |     |
|     |     |     |     |     |  S  |     |     |     |     |
|     |     |     |     |     |  O  |     |  C  |     |     |
|     |     |     |     |     |  U  |     |  H  |     |     |
|     |     |     |     |     |  R  |  P  |  A  |     |     |
|     |     |     |     |     |  I  |  O  |  T  |     |     |
|     |     |     |     |     |  S  |  U  |  R  |  A  |  T  |
|     |     |     |     |     |     |  L  |     |     |     |
|     |     |     |     |     |     |  E  |     |     |     |
|     |     |     |  C  |  H  |  I  |  E  |  N  |     |     |

### Medium level

``` r
complete_all_grid(
  words = c("CHIEN", "RAT", "SOURIS", "CHAT", "POULE"),
  size_grid = 10, difficult_level = 2
)
```

|     |     |     |     |     |     |     |     |     |     |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|  N  |  U  |  V  |  K  |  A  |  Z  |  X  |  H  |  K  |  P  |
|  K  |  A  |  Z  |  K  |  P  |  C  |  H  |  A  |  T  |  Q  |
|  U  |  V  |  I  |  H  |  V  |  L  |  S  |  B  |  F  |  N  |
|  K  |  G  |  K  |  F  |  Y  |  P  |  O  |  R  |  A  |  T  |
|  S  |  G  |  I  |  L  |  R  |  Q  |  U  |  N  |  V  |  U  |
|  Z  |  N  |  Q  |  W  |  E  |  C  |  R  |  P  |  W  |  M  |
|  T  |  E  |  P  |  Y  |  X  |  D  |  I  |  O  |  Q  |  X  |
|  V  |  I  |  H  |  X  |  D  |  W  |  S  |  U  |  R  |  K  |
|  X  |  H  |  P  |  F  |  Z  |  N  |  R  |  L  |  Z  |  W  |
|  K  |  C  |  Q  |  B  |  Y  |  B  |  Z  |  E  |  R  |  H  |

The soluce:

|     |     |     |     |     |     |     |     |     |     |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|     |     |     |     |     |     |     |     |     |     |
|     |     |     |     |     |  C  |  H  |  A  |  T  |     |
|     |     |     |     |     |     |  S  |     |     |     |
|     |     |     |     |     |     |  O  |  R  |  A  |  T  |
|     |     |     |     |     |     |  U  |     |     |     |
|     |  N  |     |     |     |     |  R  |  P  |     |     |
|     |  E  |     |     |     |     |  I  |  O  |     |     |
|     |  I  |     |     |     |     |  S  |  U  |     |     |
|     |  H  |     |     |     |     |     |  L  |     |     |
|     |  C  |     |     |     |     |     |  E  |     |     |

### High level

``` r
complete_all_grid(
  words = c("CHIEN", "RAT", "SOURIS", "CHAT", "POULE"),
  size_grid = 10, difficult_level = 3
)
```

|     |     |     |     |     |     |     |     |     |     |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|  J  |  S  |  U  |  E  |  U  |  D  |  U  |  P  |  M  |  E  |
|  N  |  N  |  I  |  R  |  D  |  F  |  K  |  O  |  H  |  E  |
|  K  |  E  |  L  |  R  |  K  |  U  |  P  |  U  |  Y  |  J  |
|  M  |  K  |  I  |  L  |  U  |  B  |  S  |  L  |  L  |  X  |
|  E  |  C  |  V  |  H  |  F  |  O  |  T  |  E  |  G  |  T  |
|  Y  |  H  |  T  |  W  |  C  |  M  |  S  |  Z  |  J  |  A  |
|  A  |  H  |  R  |  Y  |  F  |  N  |  R  |  X  |  N  |  R  |
|  X  |  O  |  B  |  X  |  G  |  Q  |  C  |  H  |  A  |  T  |
|  R  |  P  |  B  |  C  |  J  |  B  |  X  |  N  |  D  |  O  |
|  H  |  A  |  J  |  Y  |  S  |  C  |  S  |  A  |  R  |  F  |

The soluce:

|     |     |     |     |     |     |     |     |     |     |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|     |  S  |     |     |     |     |     |  P  |     |     |
|  N  |     |  I  |     |     |     |     |  O  |     |     |
|     |  E  |     |  R  |     |     |     |  U  |     |     |
|     |     |  I  |     |  U  |     |     |  L  |     |     |
|     |     |     |  H  |     |  O  |     |  E  |     |  T  |
|     |     |     |     |  C  |     |  S  |     |     |  A  |
|     |     |     |     |     |     |     |     |     |  R  |
|     |     |     |     |     |     |  C  |  H  |  A  |  T  |
|     |     |     |     |     |     |     |     |     |     |
|     |     |     |     |     |     |     |     |     |     |
